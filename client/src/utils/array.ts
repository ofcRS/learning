export const emptyArray = (length: number): null[] =>
    new Array(length).fill(null);
