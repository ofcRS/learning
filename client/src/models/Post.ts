import { types } from 'mobx-state-tree';

export const PostModel = types.model({
    __typename: types.optional(types.literal("Post"), "Post"),
    id: types.union(types.undefined, types.integer),
    title: types.union(types.undefined, types.string),
    body: types.union(types.undefined, types.string),
})