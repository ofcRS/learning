import { types } from 'mobx-state-tree';

export const UserModel = types.model({
        __typename: types.optional(types.literal("User"), "User"),
        id: types.integer,
        email: types.union(types.undefined, types.string),
        name: types.string,
    }
)