import React, { lazy } from 'react';

export type Route = {
    component: React.LazyExoticComponent<React.FC> | React.FC;
    path: string;
    name?: string;
    hideForGuests?: boolean;
};

export type TabRoute = Route & {
    label: string;
    to?: string;
};

export const routes: Route[] = [
    {
        component: lazy(() => import('pages/PostCreating')),
        path: '/new-post',
    },
    {
        component: lazy(() => import('pages/ViewPost')),
        path: '/posts/:id',
    },
];

// роуты которые отредарятся в основное меню приложения
export const mainMenuRoutes: TabRoute[] = [
    {
        name: 'Posts',
        component: lazy(() => import('pages/Posts')),
        path: '/posts',
        label: 'Posts',
    },
    {
        name: 'Messages',
        component: lazy(() => import('pages/Messages')),
        path: '/messages',
        label: 'Messages',
        hideForGuests: true,
    },
    {
        name: 'Users',
        component: lazy(() => import('pages/Users')),
        path: '/users',
        label: 'Users',
    },
    {
        component: lazy(() => import('pages/CalculusSheet')),
        path: '/calculus',
        label: 'Calculus variants sheet',
    },
];

export const allRoutes = [...routes, ...mainMenuRoutes];
