import styled from 'styled-components';

const UrlModalBody = styled.div`
    padding: 16px;
`;

export const Styled = {
    UrlModalBody,
};
