export type Props = {
    onLeaveComment: (text: string) => void;
};

export type FormValues = {
    commentText: string;
}