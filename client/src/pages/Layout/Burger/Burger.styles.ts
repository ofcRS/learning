import styled from 'styled-components';

const BurgerButton = styled.div`
    display: flex;
    margin-left: 16px;
`;

export const Styled = {
    BurgerButton,
};
