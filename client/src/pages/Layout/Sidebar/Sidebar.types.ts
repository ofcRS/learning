export type Tab = {
    path: string;
    label: string;
};
