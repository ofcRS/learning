import React from 'react';
import { observer } from 'mobx-react';

import { Styled } from './Sidebar.styles';

import { mainMenuRoutes } from 'routes';
import { useStore } from 'store';
import { Burger } from '../Burger';

export const Sidebar = observer(() => {
    const { ui, app } = useStore();

    if (!ui.sidebarOpen) return null;

    return (
        <Styled.Sidebar>
            <Styled.SidebarHeader>
                <Burger />
            </Styled.SidebarHeader>
            {mainMenuRoutes
                .filter(({ hideForGuests }) => app.user || !hideForGuests)
                .map(({ path, label, to }) => (
                    <Styled.NavLink
                        key={path}
                        to={to || path}
                    >
                        {label}
                    </Styled.NavLink>
                ))}
        </Styled.Sidebar>
    );
});
