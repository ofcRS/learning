import styled from 'styled-components';
import { Link } from 'react-router-dom';

const SearchResultsWrapper = styled.div`
    width: 200px;
    max-height: 400px;
    overflow-y: auto;

    border: 1px solid ${({ theme }) => theme.colors.neutral};
`;

const SearchResult = styled(Link)`
    display: flex;
    align-items: center;

    img {
        margin-right: 16px;
    }

    text-decoration: none;

    color: ${({ theme }) => theme.colors.neutral};
`;

export const Styled = {
    SearchResultsWrapper,
    SearchResult,
};
