import React, { useEffect, useRef, useState } from 'react';

import { GoogleLogin } from 'react-google-login';
import { useSearchPostsLazyQuery } from 'graphql/generated';
import { Callout } from 'components/Callout';
import { Styled } from './Search.styles';
import { useHistory } from 'react-router';
import { useDelayUnmount } from '../../../hooks/useDelayUnmount';
import { smoothTime } from '../../../consts/animation';

export const Search: React.FC = () => {
    const inputRef = useRef(null);
    const [searchInputValue, setSearchInputValue] = useState('');
    const [searchOnPosts, { data }] = useSearchPostsLazyQuery();

    const [showCallout, setShowCallout] = useState(false);
    const renderCallout = useDelayUnmount({
        delay: smoothTime.int,
        mounted: showCallout,
    });

    useEffect(() => {
        if (searchInputValue !== '') {
            searchOnPosts({
                variables: {
                    value: searchInputValue,
                },
            });
        }
    }, [searchInputValue, searchOnPosts]);

    return (
        <>
            <input
                ref={inputRef}
                placeholder="Search"
                value={searchInputValue}
                onChange={({ target: { value } }) => setSearchInputValue(value)}
                onFocus={() => setShowCallout(true)}
            />
            {renderCallout && (
                <Callout
                    style={{
                        zIndex: 2,
                    }}
                    target={inputRef.current}
                    onDismiss={() => {
                        setShowCallout(false);
                    }}
                    show={showCallout}
                    offset={{
                        y: 20,
                    }}
                >
                    <Styled.SearchResultsWrapper>
                        {data?.globalSearch.map(({ id, imageSrc, title }) => {
                            return (
                                <Styled.SearchResult
                                    to={'/posts/' + id}
                                    key={id}
                                >
                                    <img
                                        alt={''}
                                        width={60}
                                        height={60}
                                        src={imageSrc || ''}
                                    />
                                    {title}
                                </Styled.SearchResult>
                            );
                        })}
                    </Styled.SearchResultsWrapper>
                </Callout>
            )}
        </>
    );
};
