import styled from 'styled-components';
import { medium } from '../../../styles/breakpoints';

const UserMenu = styled.div`
    position: absolute;
    display: none;
    left: 0;
    top: 15px;
`;

const UserBlock = styled.div`
    display: flex;
    align-items: center;
    position: relative;

    color: ${({ theme }) => theme.colors.neutral};

    > span:first-child {
        @media (max-width: ${medium}) {
            display: none;
        }
        margin-right: 8px;
    }

    :hover {
        ${UserMenu} {
            display: block;
        }
    }
`;

export const Styled = {
    UserBlock,
    UserMenu,
};
