import React from 'react';
import { observer } from 'mobx-react';
import { useHistory } from 'react-router';

import { Button } from 'components/Button';

import { Burger } from '../Burger';
import { UserBlock } from '../UserBlock';
import { Search } from '../Search';

import { Props } from './Header.types';
import { Styled } from './Header.styles';

import { useStore } from 'store';
import { medium } from 'styles/breakpoints';

export const Header = observer<React.FC<Props>>(() => {
    const { app } = useStore();
    const history = useHistory();

    const onClickNewPostButton = () =>
        app.checkAuthBefore(() => history.push('/new-post'));

    const isButtonTextHidden = window.innerWidth < parseInt(medium);

    return (
        <Styled.Header>
            <Burger />
            <Search />
            <Button
                onClick={onClickNewPostButton}
                iconProps={{
                    iconName: 'plus',
                }}
            >
                {!isButtonTextHidden && 'New post'}
            </Button>
            <UserBlock />
        </Styled.Header>
    );
});
