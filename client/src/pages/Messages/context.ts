import { createContext } from 'react';
import { MessagesContext } from './Messages.types';

export const messagesContext = createContext<MessagesContext>({
    interlocutorId: null,
    onSendMessage: async () => false,
    setInterlocutorId: () => null,
    listOfUsersWithMessages: [],
    setListOfUsersWithMessages: () => null,
    messageText: '',
    setMessageText: () => null,
});
