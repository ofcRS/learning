import styled from 'styled-components';

const Messages = styled.div`
    display: flex;
    height: 100%;
    border: 1px solid ${({ theme }) => theme.colors.secondaryColor};
    max-height: calc(100vh - ${({ theme }) => theme.layout.headerHeight}px);
`;

const UsersList = styled.div`
    flex-basis: 30%;
    border: 1px solid ${({ theme }) => theme.colors.secondaryColor};
`;

const UserItem = styled.div<{ active?: boolean }>`
    padding: 8px;
    border: 1px solid ${({ theme }) => theme.colors.secondaryColor};

    cursor: pointer;
    ${({ active, theme }) =>
            active &&
            `
        background: ${theme.colors.neutral};
        color: ${theme.colors.baseBackground};
    `}
        :hover {
        background: ${({ theme }) => theme.colors.neutral};
        color: ${({ theme }) => theme.colors.baseBackground};
    }
`;

const DialogArea = styled.div`
    width: 70%;
`;

const History = styled.div`
    height: 100%;
`;

const ChatWrapper = styled.div`
    height: 100%;
    display: flex;
    flex-direction: column;
`;

const Message = styled.div`
    display: flex;
    flex-wrap: wrap;
    border: 1px solid ${({ theme }) => theme.colors.neutral};
    padding: 4px;

    p {
        display: flex;
        width: 100%;
        justify-content: space-between;

        margin-bottom: 8px;
    }

    div {
        font-size: 20px;
        line-break: anywhere;
    }

    :not(:last-child) {
        margin-bottom: 8px;
    }
`;

const ChatMessagesWrapper = styled.div`
    width: 100%;
    padding: 16px;
    overflow-y: auto;
`;

const SendMessage = styled.div`
    width: 100%;
    padding: 4px;
    border-top: 1px solid ${({ theme }) => theme.colors.neutral};

    display: flex;

    margin-top: auto;

    textarea {
        width: 100%;
        padding: 16px;
        margin-right: 8px;

        font-size: 16px;
        color: ${({ theme }) => theme.colors.neutral};
        background: none;

        resize: none;
    }
`;

const SelectInterlocutorTip = styled.div`
    display: flex;
    width: 100%;
    height: 100%;
    align-items: center;
    justify-content: center;

    i {
        margin-right: 8px;
        svg {
            width: 24px;
            fill: ${({ theme }) => theme.colors.neutral};
        }
    }
`;

export const Styled = {
    UsersList,
    Messages,
    DialogArea,
    SendMessage,
    History,
    UserItem,
    Message,
    SelectInterlocutorTip,
    ChatWrapper,
    ChatMessagesWrapper,
};
