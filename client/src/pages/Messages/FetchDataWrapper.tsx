import React, { useCallback, useEffect, useState } from 'react';
import { useStore } from '../../store';
import { useLocation } from 'react-router';

import {
    GetMessagesDocument,
    GetMessagesQuery,
    useGetUsersListQuery,
    useSendMessageMutation,
    useSubscribeToMessagesSubscription,
    useUserLazyQuery,
} from 'graphql/generated';

import { messagesContext } from './context';
import { Messages } from './Messages';
import { OnSendMessage, UserWithMessages } from './Messages.types';

const removeDuplicateUsers = (users: UserWithMessages[]): UserWithMessages[] =>
    users.reduce<UserWithMessages[]>(
        (res, cur) =>
            res.find(({ id }) => id === cur.id) ? [...res] : [...res, cur],
        []
    );

export const FetchDataWrapper: React.FC = () => {
    const { app } = useStore();
    const [interlocutorId, setInterlocutorId] = useState<number | null>(null);
    const [messageText, setMessageText] = useState('');

    const [send, { client }] = useSendMessageMutation({
        onCompleted: ({ sendMessage }) => {
            const user = app.user;
            if (!user) return;
            const data =
                client?.readQuery<GetMessagesQuery>({
                    query: GetMessagesDocument,
                    variables: { interlocutorId },
                })?.getMessages || [];

            client?.writeQuery<GetMessagesQuery>({
                query: GetMessagesDocument,
                data: {
                    getMessages: [
                        ...data,
                        {
                            ...sendMessage,
                            from: {
                                name: user.name,
                                id: user.id,
                            },
                        },
                    ],
                },
                variables: {
                    interlocutorId,
                },
            });
        },
    });

    const [listOfUsersWithMessages, setListOfUsersWithMessages] = useState<
        UserWithMessages[]
    >([]);

    const { data } = useGetUsersListQuery({
        fetchPolicy: 'cache-and-network'
    });

    useEffect(() => {
        setListOfUsersWithMessages(prev =>
            removeDuplicateUsers([
                ...prev,
                ...(data?.getListOfUsersWithMessages || []),
            ])
        );
    }, [data?.getListOfUsersWithMessages]);

    const [getUserInfo] = useUserLazyQuery({
        onCompleted: data => {
            setListOfUsersWithMessages(prev =>
                removeDuplicateUsers([...prev, { ...data.getUserInfo }])
            );
        },
    });

    useSubscribeToMessagesSubscription({
        onSubscriptionData: ({ subscriptionData, client }) => {
            if (!subscriptionData.data?.subscribeToMessage) return;
            const { from } = subscriptionData.data.subscribeToMessage;

            setListOfUsersWithMessages(prev =>
                removeDuplicateUsers([
                    ...prev,
                    {
                        name: from.name,
                        id: from.id,
                    },
                ])
            );
            const data =
                client.readQuery<GetMessagesQuery>({
                    query: GetMessagesDocument,
                    variables: { interlocutorId: from.id },
                })?.getMessages || [];
            client.writeQuery<GetMessagesQuery>({
                query: GetMessagesDocument,
                data: {
                    getMessages: [
                        ...data,
                        subscriptionData.data.subscribeToMessage,
                    ],
                },
                variables: {
                    interlocutorId: from.id,
                },
            });
        },
    });

    const onSendMessage = useCallback<OnSendMessage>(
        async text => {
            if (interlocutorId !== null && text) {
                await send({
                    variables: {
                        text,
                        to: interlocutorId,
                    },
                });
                setMessageText('');
            }
            return true;
        },
        [interlocutorId, send]
    );

    const location = useLocation();

    const initUsersList = useCallback(async () => {
        const params = new URLSearchParams(location.search);
        const interlocutorId = parseInt(params.get('interlocutorId')!);

        if (!Number.isNaN(interlocutorId)) {
            setInterlocutorId(interlocutorId);
            getUserInfo({ variables: { id: interlocutorId } });
        }
    }, [getUserInfo, location.search]);

    useEffect(() => {
        initUsersList();
    }, [initUsersList]);

    return (
        <messagesContext.Provider
            value={{
                interlocutorId,
                onSendMessage,
                setInterlocutorId,
                setListOfUsersWithMessages,
                listOfUsersWithMessages,
                setMessageText,
                messageText,
            }}
        >
            <Messages />
        </messagesContext.Provider>
    );
};
