import { SetStateAction, Dispatch } from 'react';

export type Props = {};

export type DialogProps = {
    interlocutorId: number;
};

export type OnSendMessage = (text: string) => Promise<boolean>;

export type UserWithMessages = {
    id: number;
    name: string;
};

export type MessagesContext = {
    interlocutorId: number | null;
    setInterlocutorId: Dispatch<SetStateAction<number | null>>;
    onSendMessage: OnSendMessage;
    listOfUsersWithMessages: UserWithMessages[];
    setListOfUsersWithMessages: Dispatch<SetStateAction<UserWithMessages[]>>;
    messageText: string;
    setMessageText: Dispatch<SetStateAction<string>>;
};
