import React, { useContext } from 'react';

import { Props } from './Messages.types';
import { Styled } from './Messages.styles';

import { Dialog } from './Dialog';

import { messagesContext } from './context';
import { Icon } from 'components/Icon';

export const Messages: React.FC<Props> = () => {
    const {
        setInterlocutorId,
        interlocutorId,

        listOfUsersWithMessages,
    } = useContext(messagesContext);

    let dialogArea = (
        <Styled.SelectInterlocutorTip>
            <Icon iconName="info" />
            <span>
                Select a chat or go to &apos;Users&apos; page to start messaging
            </span>
        </Styled.SelectInterlocutorTip>
    );

    if (interlocutorId !== null) {
        dialogArea = <Dialog interlocutorId={interlocutorId} />;
    }

    return (
        <Styled.Messages>
            <Styled.UsersList>
                {listOfUsersWithMessages.map(({ name, id }) => (
                    <Styled.UserItem
                        active={interlocutorId === id}
                        onClick={() => setInterlocutorId(id)}
                        key={id}
                    >
                        {name}
                    </Styled.UserItem>
                ))}
            </Styled.UsersList>
            <Styled.DialogArea>{dialogArea}</Styled.DialogArea>
        </Styled.Messages>
    );
};
