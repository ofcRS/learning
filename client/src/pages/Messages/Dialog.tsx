import React, { useContext, useEffect, useRef } from 'react';
import { parseISO, format } from 'date-fns';

import { DialogProps } from './Messages.types';
import { useGetMessagesQuery } from 'graphql/generated';
import { Styled } from './Messages.styles';
import { messagesContext } from './context';
import { Button } from '../../components/Button';

export const Dialog: React.FC<DialogProps> = ({ interlocutorId }) => {
    const {
        onSendMessage,

        messageText,
        setMessageText,
    } = useContext(messagesContext);

    const chatWrapperRef = useRef<HTMLDivElement>(null);

    const { data } = useGetMessagesQuery({
        variables: {
            interlocutorId,
        },
    });

    const scrollToBottomOfChat = () => {
        const chatWrapper = chatWrapperRef.current;
        if (chatWrapper) {
            chatWrapper.scrollTo({
                behavior: 'smooth',
                top: chatWrapper.scrollHeight - chatWrapper.clientHeight,
            });
        }
    };

    const onSend = async () => {
        await onSendMessage(messageText);
        scrollToBottomOfChat();
    };

    return (
        <Styled.ChatWrapper>
            <Styled.ChatMessagesWrapper ref={chatWrapperRef}>
                {data?.getMessages.map(({ id, from, text, createdAt }) => (
                    <Styled.Message key={id}>
                        <p>
                            <b>{from.name}</b>
                            <i>
                                {format(
                                    parseISO(createdAt),
                                    'dd.MM.yyyy hh:mm'
                                )}
                            </i>
                        </p>
                        <div>{text}</div>
                    </Styled.Message>
                ))}
            </Styled.ChatMessagesWrapper>
            <Styled.SendMessage>
                <textarea
                    onChange={({ target }) => setMessageText(target.value)}
                    value={messageText}
                    placeholder="Enter a message..."
                />
                <Button onClick={onSend}>Send</Button>
            </Styled.SendMessage>
        </Styled.ChatWrapper>
    );
};
