import React from 'react';

import { User } from './User';

import { Props } from './Users.types';

import { useUsersQuery } from 'graphql/generated';
import { useHistory } from 'react-router';

export const Users: React.FC<Props> = () => {
    const { data } = useUsersQuery();

    const history = useHistory();

    const handleGoToMessage = (id: number) => {
        history.push('/messages?interlocutorId=' + id);
    };

    return (
        <div>
            {data?.users.map(user => (
                <User
                    key={user.id}
                    user={user}
                    onGoToMessage={handleGoToMessage}
                />
            ))}
        </div>
    );
};
