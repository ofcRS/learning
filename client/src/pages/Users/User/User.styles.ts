import styled from 'styled-components';

const User = styled.div`
    display: flex;
    align-items: center;
    
    padding: 16px;
    border: 1px solid ${({ theme }) => theme.colors.neutral};
    
    
    button {
        margin-left: auto;
    }
`;

export const Styled = {
    User,
};
