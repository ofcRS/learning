import React from 'react';

import { Styled } from './User.styles';
import { Props } from './User.types';
import { Button } from 'components/Button';
import { useStore } from '../../../store';

export const User: React.FC<Props> = ({ user, onGoToMessage }) => {
    const { app } = useStore();

    const showSendMessageButton = app.user && app.user.id !== user.id;

    return (
        <Styled.User>
            <b>{user.name}</b>
            {showSendMessageButton && (
                <Button
                    iconProps={{
                        iconName: 'message',
                    }}
                    onClick={() => onGoToMessage(user.id)}
                >
                    Send message
                </Button>
            )}
        </Styled.User>
    );
};
