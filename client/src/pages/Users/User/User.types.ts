import { User } from 'graphql/generated';

export type Props = {
    user: User;
    onGoToMessage: (id: number) => void;
};
