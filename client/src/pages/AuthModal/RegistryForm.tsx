import React from 'react';
import { observer } from 'mobx-react';
import { Form, Formik } from 'formik';

import { Styled } from './AuthModal.styles';

import { parseGraphQLError } from 'utils/validators';

import { ErrorMessage } from 'components/FormError';
import { MeDocument, MeQuery, useRegisterMutation } from 'graphql/generated';
import { RegistryFormProps } from './AuthModal.types';
import { useStore } from 'store';
import { Button, ButtonVariant } from '../../components/Button';
import { registryFormValidation } from './validate';
import { TextField } from '../../components/TextField';

export const RegistryForm = observer<React.FC<RegistryFormProps>>(
    ({ onBackToLogin }) => {
        const { app } = useStore();

        const [registry, { error }] = useRegisterMutation({
            onCompleted: ({ register }) => app.login(register),
            update: (store, { data }) => {
                if (!data) return;
                store.writeQuery<MeQuery>({
                    query: MeDocument,
                    data: {
                        me: data.register.user,
                    },
                });
            },
        });

        return (
            <Formik
                validationSchema={registryFormValidation}
                onSubmit={async ({ password, email, name }) => {
                    await registry({
                        variables: {
                            email,
                            password,
                            name,
                        },
                    });
                }}
                initialValues={{
                    email: '',
                    password: '',
                    name: '',
                }}
            >
                {({ isSubmitting }) => (
                    <Form>
                        {error && (
                            <ErrorMessage>
                                {parseGraphQLError(error)}
                            </ErrorMessage>
                        )}
                        <Styled.InputWrapper>
                            <TextField name="email" label="E-mail" />
                        </Styled.InputWrapper>
                        <Styled.InputWrapper>
                            <TextField label="Name" name="name" />
                        </Styled.InputWrapper>
                        <Styled.InputWrapper>
                            <TextField
                                type="password"
                                name="password"
                                label="Password"
                            />
                        </Styled.InputWrapper>
                        <Styled.ButtonsWrapper>
                            <Button
                                variant={ButtonVariant.submit}
                                type="submit"
                                disabled={isSubmitting}
                            >
                                Submit
                            </Button>
                            <Button
                                variant={ButtonVariant.text}
                                onClick={() => onBackToLogin()}
                            >
                                Log in
                            </Button>
                        </Styled.ButtonsWrapper>
                    </Form>
                )}
            </Formik>
        );
    }
);
