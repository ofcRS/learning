import React from 'react';
import { Form, Formik } from 'formik';
import { observer } from 'mobx-react';
import { Styled } from './AuthModal.styles';
import { LoginFormProps } from './AuthModal.types';
import { useLoginMutation } from '../../graphql/generated';
import { useStore } from 'store';
import { parseGraphQLError } from 'utils/validators';
import { ErrorMessage } from 'components/FormError';
import { Button, ButtonVariant } from 'components/Button';
import { loginFormValidation } from './validate';
import { TextField } from '../../components/TextField';

export const LoginForm = observer<React.FC<LoginFormProps>>(({ onSignIn }) => {
    const { app } = useStore();

    const [login, { error }] = useLoginMutation({
        errorPolicy: 'all',
        onCompleted: data => data?.login && app.login(data.login),
    });

    return (
        <Formik
            validationSchema={loginFormValidation}
            onSubmit={values =>
                login({
                    variables: values,
                })
            }
            initialValues={{
                email: '',
                password: '',
            }}
        >
            {({ isSubmitting, errors }) => (
                <Form>
                    {error && (
                        <ErrorMessage>{parseGraphQLError(error)}</ErrorMessage>
                    )}
                    <Styled.InputWrapper>
                        <TextField name="email" label="E-mail" />
                    </Styled.InputWrapper>
                    <Styled.InputWrapper>
                        <TextField
                            type="password"
                            name="password"
                            label="Password"
                        />
                    </Styled.InputWrapper>
                    <Styled.ButtonsWrapper>
                        <Button
                            variant={ButtonVariant.submit}
                            type="submit"
                            disabled={isSubmitting}
                        >
                            Submit
                        </Button>
                        <Button
                            variant={ButtonVariant.text}
                            onClick={() => onSignIn()}
                        >
                            Sign In
                        </Button>
                    </Styled.ButtonsWrapper>
                </Form>
            )}
        </Formik>
    );
});
