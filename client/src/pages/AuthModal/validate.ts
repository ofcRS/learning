import * as Yup from 'yup';

export const loginFormValidation = Yup.object().shape({
    email: Yup.string()
        .email('Enter valid email')
        .required('Required field'),
    password: Yup.string().required('Required field'),
});

export const registryFormValidation = Yup.object().shape({
    email: Yup.string()
        .email('Enter valid email')
        .required('Required field'),
    password: Yup.string().required('Required field'),
    name: Yup.string().required('Required field'),
});
