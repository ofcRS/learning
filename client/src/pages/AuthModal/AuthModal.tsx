import React from 'react';
import { observer, useLocalStore } from 'mobx-react';
import {
    GoogleLogin,
    GoogleLoginResponse,
    GoogleLoginResponseOffline,
} from 'react-google-login';

import { Modal } from 'components/Modal';

import { LoginForm } from './LoginForm';
import { RegistryForm } from './RegistryForm';

import { Styled } from './AuthModal.styles';
import { AuthModalLocalStore, AuthModalMode } from './AuthModal.types';

import { useStore } from 'store';
import { request } from '../../utils/request';
import { LoginResponse } from '../../graphql/generated';

export const ModalBody = observer(() => {
    const localStore = useLocalStore<AuthModalLocalStore>(() => ({
        mode: AuthModalMode.login,
    }));

    const { app } = useStore();

    const title = localStore.mode === AuthModalMode.login ? 'Login' : 'Join';
    const form =
        localStore.mode === AuthModalMode.login ? (
            <LoginForm
                onSignIn={() => (localStore.mode = AuthModalMode.registry)}
            />
        ) : (
            <RegistryForm
                onBackToLogin={() => (localStore.mode = AuthModalMode.login)}
            />
        );

    const onSuccess = async (
        googleData: GoogleLoginResponse | GoogleLoginResponseOffline
    ) => {
        try {
            const data = await request<LoginResponse>({
                method: 'post',
                body: {
                    token: (googleData as GoogleLoginResponse).tokenId,
                },
                url: '/auth/google',
            });
            console.log(data);
            app.login(data);
        } catch (error) {
            console.log({ ...error });
        }
    };

    return (
        <Styled.FormWrapper>
            <Styled.Title>{title}</Styled.Title>
            {form}
            <GoogleLogin
                theme="dark"
                onSuccess={onSuccess}
                onFailure={console.log}
                clientId="399052810448-u92l9fviei84v1l83pgaesepqq94emh4.apps.googleusercontent.com"
                isSignedIn={false}
            />
        </Styled.FormWrapper>
    );
});

export const AuthModal = observer(() => {
    const { ui } = useStore();

    // выношу тело в отдельный компонент, чтобы он анмаунтился при закрытие модалки
    return (
        <Modal
            onClose={() => ui.toggleRegistryModal(false)}
            open={ui.registryModalOpen}
        >
            <ModalBody />
        </Modal>
    );
});
