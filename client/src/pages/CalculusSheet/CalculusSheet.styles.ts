import styled from 'styled-components';

const Wrapper = styled.div`
    display: flex;
    flex-direction: column;

    width: 40vw;
    margin: 0 auto;
    font-size: 36px;

`;

const InputWrapper = styled.div`
    display: flex;

    padding: 16px;

    column-gap: 32px;
    align-items: center;

    & input {
        padding: 8px;
        font-size: 16px;
        
        background: none;
        border: 1px solid #fff;
        color: #fff;
    }
`;

const Table = styled.table`
    border-collapse: collapse;

    & td {
        justify-content: center;
        align-items: center;
        padding: 8px;
        text-align: center;

        border: 1px solid #fff;

        & input {
            height: 32px;
            width: 32px;
        }
    }
`;

export const Styled = {
    Wrapper,
    Table,
    InputWrapper,
};
