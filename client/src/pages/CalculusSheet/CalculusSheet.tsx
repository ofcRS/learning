import React, { useEffect, useState } from 'react';

import { Styled } from './CalculusSheet.styles';
import { Props } from './CalculusSheet.types';
import { sequence, TASKS_COUNT } from './config';

export const CalculusSheet: React.FC<Props> = () => {
    const [variant, setVariant] = useState(4);
    const [completeness, setCompleteness] = useState<Record<string, boolean>>(
        {}
    );

    useEffect(() => {
        setCompleteness(JSON.parse(localStorage.getItem('sheet')) || {});
        setVariant(parseInt(localStorage.getItem('var')) || 4);
    }, []);

    useEffect(() => {
        localStorage.setItem('sheet', JSON.stringify(completeness));
        localStorage.setItem('var', `${variant}`);
    }, [completeness, variant]);

    return (
        <Styled.Wrapper>
            <Styled.InputWrapper>
                <label>Вариант</label>
                <input
                    type="number"
                    value={variant}
                    onChange={({ target }) => {
                        const parsed = parseInt(target.value);

                        if (Number.isNaN(parsed)) {
                            setVariant(1);
                        } else {
                            setVariant(parsed);
                        }
                    }}
                />
            </Styled.InputWrapper>
            <Styled.Table>
                <thead>
                    <tr>
                        <td>Задание</td>
                        <td>Номер</td>
                        <td>Прогресс</td>
                    </tr>
                </thead>
                <tbody>
                    {Array.from({ length: TASKS_COUNT }).map((_, index) => {
                        const number = (index + variant - 1) % sequence.length;

                        const checked = completeness[index];

                        return (
                            <tr
                                key={index}
                                style={{
                                    background: checked ? 'green' : 'unset',
                                }}
                            >
                                <td>{index + 1}</td>
                                <td>{sequence[number]}</td>
                                <td>
                                    <input
                                        checked={checked}
                                        onChange={() => {
                                            setCompleteness(prevState => ({
                                                ...prevState,
                                                [index]: !prevState[index],
                                            }));
                                        }}
                                        type="checkbox"
                                    />
                                </td>
                            </tr>
                        );
                    })}
                </tbody>
            </Styled.Table>
        </Styled.Wrapper>
    );
};
