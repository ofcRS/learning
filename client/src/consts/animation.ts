export const smoothTiming = 'cubic-bezier(0.1, 0.9, 0.2, 1)';
export const smoothTime = {
    ms: '300ms',
    int: 300,
};
