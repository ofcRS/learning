import styled from 'styled-components';

import { Props } from './PostPreview.types';

import { smoothTime } from 'consts/animation';
import { medium } from 'styles/breakpoints';

const PostPreview = styled.div<Props>`
    position: absolute;
    z-index: 1;

    width: 100%;
    height: 100%;

    background: ${({ theme }) => theme.colors.dark};

    left: ${({ show }) => (show ? '50%' : '100%')};
    transform: translateX(${({ show }) => (show ? '-50%' : '100%')});

    overflow: hidden;

    transition: all ${smoothTime.ms} linear;
`;

const Wrapper = styled.div`
    max-width: 680px;
    margin: 0 auto;

    @media (min-width: ${medium}) {
        width: 50vw;
    }
`;

const ImagePreview = styled.div`
    margin: 0 auto 16px;

    @media (min-width: ${medium}) {
        width: 30vw;
    }
`;

export const Styled = {
    PostPreview,
    ImagePreview,
    Wrapper,
};
