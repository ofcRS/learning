import styled from 'styled-components';

const Icon = styled.i`
    vertical-align: middle;
`;

export const Styled = {
    Icon,
};
