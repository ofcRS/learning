import { DOMAttributes } from 'react';

export type Props = {
    iconName: string;
} & DOMAttributes<HTMLElement>;
