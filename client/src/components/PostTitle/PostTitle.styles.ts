import styled from 'styled-components';

export const PostTitle = styled.h1`
    font-size: 48px;
    line-height: 64px;
    margin-bottom: 16px;
`;

export const Styled = {
    PostTitle,
};
