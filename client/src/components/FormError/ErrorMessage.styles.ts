import styled, { keyframes } from 'styled-components';
import { smoothTime, smoothTiming } from 'consts/animation';

const fadeIn = keyframes`
    0% {
        padding: 0;
        opacity: 0;
    }
    100% {
        padding: 10px 0;
        opacity: 1;
    }
`;

const ErrorMessageContainer = styled.div`
    padding: 10px 0;

    font-size: 14px;
    color: ${({ theme }) => theme.colors.pastel[0]};

    animation-name: ${fadeIn};
    animation-timing-function: ${smoothTiming};
    animation-duration: ${smoothTime.ms};
`;

const ErrorMessage = styled.div``;

export const Styled = {
    ErrorMessageContainer,
    ErrorMessage,
};
