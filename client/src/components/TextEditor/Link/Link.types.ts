export type Props = {
    offsetKey: string;
    entityKey: string;
    readonly?: boolean;
};
