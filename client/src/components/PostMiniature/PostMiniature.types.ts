import { PostPreview } from 'graphql/generated';

export type Props = {
    post: PostPreview;
};
