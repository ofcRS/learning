import styled from 'styled-components';

const Label = styled.label``;

const TextField = styled.div<{ hasError?: boolean }>`
    width: 100%;

    display: flex;
    flex-wrap: wrap;

    ${Label} {
        width: 100%;
        margin-bottom: 8px;

        ${({ hasError, theme }) =>
            hasError && `color:${theme.colors.pastel[0]}`}
    }

    input {
        width: 100%;
        padding: 8px;

        color: ${({ theme }) => theme.colors.neutral};

        background: none;

        border: 1px solid
            ${({ hasError, theme }) =>
                hasError
                    ? theme.colors.pastel[0]
                    : theme.colors.secondaryColor};
    }
`;

export const Styled = {
    TextField,
    Label,
};
