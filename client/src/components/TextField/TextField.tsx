import React from 'react';
import { useField, Field } from 'formik';

import { ErrorMessage } from 'components/FormError';

import { Styled } from './TextField.styles';
import { Props } from './TextField.types';

export const TextField: React.FC<Props> = ({ name, label, ...props }) => {
    const [_, { error, touched }] = useField(name);

    const hasError = touched && typeof error === 'string';

    return (
        <Styled.TextField hasError={hasError}>
            <Styled.Label htmlFor={name}>{label}</Styled.Label>
            <Field id={name} name={name} {...props} />
            {hasError && <ErrorMessage>{error}</ErrorMessage>}
        </Styled.TextField>
    );
};
