export type Props = {
    onClick: () => void;
    show: boolean;
};
