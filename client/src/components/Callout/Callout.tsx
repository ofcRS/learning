import React, { useCallback, useEffect, useRef, useState } from 'react';
import { createPortal } from 'react-dom';

import { Styled } from './Callout.styles';
import { Props, Position } from './Callout.types';
import { body } from 'links';

export const Callout: React.FC<Props> = ({
    children,
    target,
    onDismiss,
    ...props
}: Props) => {
    const [position, setPosition] = useState<Position>({
        x: 0,
        y: 0,
    });

    const calloutRef = useRef<HTMLDivElement>(null);

    useEffect(() => {
        window.addEventListener('resize', () => onDismiss());
        return () => {
            window.removeEventListener('resize', onDismiss);
        };
    }, [onDismiss]);

    const handleClickOutsideCallout = useCallback(
        (event: MouseEvent) => {
            const callout = calloutRef.current;
            if (
                event.target instanceof Element &&
                callout &&
                !event.target.contains(callout) &&
                event.target !== target
            ) {
                onDismiss();
            }
        },
        [onDismiss, target]
    );

    const updatePosition = useCallback(() => {
        const callout = calloutRef.current;
        if (target && callout) {
            const yOffset = window.pageYOffset;
            const xOffset = window.pageXOffset;

            const targetWidth = target.offsetWidth;
            const targetHeight = target.offsetHeight;

            const targetBounding = target.getBoundingClientRect();
            const calloutBounding = callout.getBoundingClientRect();

            const y =
                yOffset +
                targetBounding.top +
                targetHeight +
                (props?.offset?.y || 0);
            let x = xOffset + targetBounding.left + (props?.offset?.x || 0);

            // Если слева от таргета нет места рендерим справа
            if (calloutBounding.width + x > window.innerWidth) {
                x -= calloutBounding.width;
            }

            setPosition({ y, x });
        }
    }, [props?.offset?.x, props?.offset?.y, target]);

    useEffect(() => {
        document.addEventListener('scroll', updatePosition);
        return () => {
            document.removeEventListener('scroll', updatePosition);
        };
    }, [updatePosition]);

    useEffect(() => {
        updatePosition();
    }, [updatePosition]);

    useEffect(() => {
        document.addEventListener('click', handleClickOutsideCallout);
        return () =>
            document.removeEventListener('click', handleClickOutsideCallout);
    }, [handleClickOutsideCallout]);

    return createPortal(
        <Styled.Callout ref={calloutRef} {...position} {...props}>
            {children}
        </Styled.Callout>,
        body
    );
};
