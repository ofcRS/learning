import React from 'react';

import { Icon } from 'components/Icon';

import { Styled } from './Button.styles';
import { Props } from './Button.types';

export const Button: React.FC<Props> = ({ children, iconProps, ...props }) => {
    const noText = !children;
    return (
        <Styled.Button noText={noText} {...props}>
            {iconProps && <Icon {...iconProps} />}
            <span>{children}</span>
        </Styled.Button>
    );
};
