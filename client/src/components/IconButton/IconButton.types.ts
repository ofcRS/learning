import { IconProps } from 'components/Icon';

export type Props = {
    iconProps: IconProps;
    onClick?: () => void;
};
