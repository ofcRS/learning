export const theme = {
    layout: {
        headerHeight: 56,
    },
    colors: {
        primaryColor: '#2b2d42',
        secondaryColor: '#8d99ae',
        baseBackground: '#1F2223',
        neutral: '#edf2f4',
        active: '#ef233c',
        activeBrighter: '#ff1e28',
        action: '#1A549E',
        actionBrighter: '#1a54be',
        dark: '#181a1b',
        white: '#ffffff',

        pastel: ['#f25f5c', '#ffe066', '#247ba0', '#289d93'],
    },
} as const;

export type Theme = typeof theme;