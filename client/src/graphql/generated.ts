import { gql } from '@apollo/client';
import * as ApolloReactCommon from '@apollo/client';
import * as ApolloReactHooks from '@apollo/client';
export type Maybe<T> = T | null;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
const defaultOptions =  {}
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  /** The javascript `Date` as string. Type represents date and time as the ISO Date string. */
  DateTime: any;
};

export type ApiResponse = {
  __typename?: 'ApiResponse';
  error: Scalars['String'];
  ok: Scalars['Boolean'];
};

export type Comment = {
  __typename?: 'Comment';
  id: Scalars['Int'];
  post: Post;
  user: User;
  text: Scalars['String'];
  replyId?: Maybe<Scalars['Float']>;
  replies?: Maybe<Array<Comment>>;
  createdAt: Scalars['DateTime'];
  updatedAt: Scalars['DateTime'];
};


export type LoginResponse = {
  __typename?: 'LoginResponse';
  accessToken: Scalars['String'];
  user: User;
};

export type Message = {
  __typename?: 'Message';
  id: Scalars['Int'];
  from: User;
  to: User;
  text: Scalars['String'];
  createdAt: Scalars['DateTime'];
  updatedAt: Scalars['DateTime'];
};

export enum Mutability {
  Mutable = 'MUTABLE',
  Immutable = 'IMMUTABLE',
  Segmented = 'SEGMENTED'
}

export type Mutation = {
  __typename?: 'Mutation';
  addPost: Post;
  deletePost: ApiResponse;
  leaveComment: Comment;
  revokeRefreshTokens: Scalars['Boolean'];
  login: LoginResponse;
  register: LoginResponse;
  sendMessage: Message;
};


export type MutationAddPostArgs = {
  body: PostBodyInput;
  title: Scalars['String'];
};


export type MutationDeletePostArgs = {
  id: Scalars['Float'];
};


export type MutationLeaveCommentArgs = {
  replyId?: Maybe<Scalars['Int']>;
  postId: Scalars['Int'];
  text: Scalars['String'];
};


export type MutationRevokeRefreshTokensArgs = {
  id: Scalars['Float'];
};


export type MutationLoginArgs = {
  password: Scalars['String'];
  email: Scalars['String'];
};


export type MutationRegisterArgs = {
  name: Scalars['String'];
  password: Scalars['String'];
  email: Scalars['String'];
};


export type MutationSendMessageArgs = {
  to: Scalars['Int'];
  text: Scalars['String'];
};

export type Post = {
  __typename?: 'Post';
  id: Scalars['Int'];
  title: Scalars['String'];
  body: PostBody;
  comments: Array<Comment>;
  createdAt: Scalars['DateTime'];
  updatedAt: Scalars['DateTime'];
};

export type PostPreview = {
  __typename?: 'PostPreview';
  id: Scalars['Float'];
  title: Scalars['String'];
  bodyPreview: Scalars['String'];
  imageSrc?: Maybe<Scalars['String']>;
  userId: Scalars['Int'];
};

export type Query = {
  __typename?: 'Query';
  posts: Array<Post>;
  getPost?: Maybe<Post>;
  postsPreview: Array<PostPreview>;
  getAmountOfPosts: Scalars['Int'];
  globalSearch: Array<PostPreview>;
  hello: Scalars['String'];
  users: Array<User>;
  me?: Maybe<User>;
  getUserInfo: User;
  getMessages: Array<Message>;
  getListOfUsersWithMessages: Array<User>;
};


export type QueryGetPostArgs = {
  id: Scalars['Int'];
};


export type QueryPostsPreviewArgs = {
  take: Scalars['Int'];
  skip: Scalars['Int'];
};


export type QueryGlobalSearchArgs = {
  value: Scalars['String'];
};


export type QueryGetUserInfoArgs = {
  id: Scalars['Int'];
};


export type QueryGetMessagesArgs = {
  interlocutorId: Scalars['Int'];
};

export type Subscription = {
  __typename?: 'Subscription';
  subscribeToMessage: Message;
};

export type User = {
  __typename?: 'User';
  id: Scalars['Int'];
  email: Scalars['String'];
  name: Scalars['String'];
};

export type ContentBlock = {
  __typename?: 'contentBlock';
  key: Scalars['String'];
  type: Scalars['String'];
  text: Scalars['String'];
  depth: Scalars['Float'];
  inlineStyleRanges: Array<InlineStyleRange>;
  entityRanges: Array<EntityRange>;
};

export type ContentBlockInput = {
  key: Scalars['String'];
  type: Scalars['String'];
  text: Scalars['String'];
  depth: Scalars['Float'];
  inlineStyleRanges: Array<InlineStyleRangeInput>;
  entityRanges: Array<EntityRangeInput>;
};

export type EntityData = {
  __typename?: 'entityData';
  src?: Maybe<Scalars['String']>;
};

export type EntityDataInput = {
  src?: Maybe<Scalars['String']>;
};

export type EntityMap = {
  __typename?: 'entityMap';
  mutability: Mutability;
  type: EntityType;
  data: EntityData;
};

export type EntityMapInput = {
  mutability: Mutability;
  type: EntityType;
  data: EntityDataInput;
};

export type EntityRange = {
  __typename?: 'entityRange';
  key: Scalars['Float'];
  offset: Scalars['Float'];
  length: Scalars['Float'];
};

export type EntityRangeInput = {
  key: Scalars['Float'];
  offset: Scalars['Float'];
  length: Scalars['Float'];
};

export enum EntityType {
  Image = 'IMAGE',
  Link = 'LINK'
}

export type InlineStyleRange = {
  __typename?: 'inlineStyleRange';
  style: Scalars['String'];
  offset: Scalars['Float'];
  length: Scalars['Float'];
};

export type InlineStyleRangeInput = {
  style: Scalars['String'];
  offset: Scalars['Float'];
  length: Scalars['Float'];
};

export type PostBody = {
  __typename?: 'postBody';
  blocks: Array<ContentBlock>;
  entityMap: Array<EntityMap>;
};

export type PostBodyInput = {
  blocks: Array<ContentBlockInput>;
  entityMap: Array<EntityMapInput>;
};

export type HelloQueryVariables = Exact<{ [key: string]: never; }>;


export type HelloQuery = (
  { __typename?: 'Query' }
  & Pick<Query, 'hello'>
);

export type BaseMessageFragment = (
  { __typename?: 'Message' }
  & Pick<Message, 'id' | 'createdAt' | 'updatedAt' | 'text'>
  & { from: (
    { __typename?: 'User' }
    & Pick<User, 'id' | 'name'>
  ) }
);

export type SubscribeToMessagesSubscriptionVariables = Exact<{ [key: string]: never; }>;


export type SubscribeToMessagesSubscription = (
  { __typename?: 'Subscription' }
  & { subscribeToMessage: (
    { __typename?: 'Message' }
    & BaseMessageFragment
  ) }
);

export type GetUsersListQueryVariables = Exact<{ [key: string]: never; }>;


export type GetUsersListQuery = (
  { __typename?: 'Query' }
  & { getListOfUsersWithMessages: Array<(
    { __typename?: 'User' }
    & Pick<User, 'id' | 'name'>
  )> }
);

export type SendMessageMutationVariables = Exact<{
  text: Scalars['String'];
  to: Scalars['Int'];
}>;


export type SendMessageMutation = (
  { __typename?: 'Mutation' }
  & { sendMessage: (
    { __typename?: 'Message' }
    & Pick<Message, 'createdAt' | 'updatedAt' | 'id' | 'text'>
  ) }
);

export type GetMessagesQueryVariables = Exact<{
  interlocutorId: Scalars['Int'];
}>;


export type GetMessagesQuery = (
  { __typename?: 'Query' }
  & { getMessages: Array<(
    { __typename?: 'Message' }
    & BaseMessageFragment
  )> }
);

export type BodyFragment = (
  { __typename?: 'postBody' }
  & { blocks: Array<(
    { __typename?: 'contentBlock' }
    & Pick<ContentBlock, 'depth' | 'key' | 'text' | 'type'>
    & { entityRanges: Array<(
      { __typename?: 'entityRange' }
      & Pick<EntityRange, 'key' | 'length' | 'offset'>
    )>, inlineStyleRanges: Array<(
      { __typename?: 'inlineStyleRange' }
      & Pick<InlineStyleRange, 'length' | 'offset' | 'style'>
    )> }
  )>, entityMap: Array<(
    { __typename?: 'entityMap' }
    & Pick<EntityMap, 'mutability' | 'type'>
    & { data: (
      { __typename?: 'entityData' }
      & Pick<EntityData, 'src'>
    ) }
  )> }
);

export type PostCommentFragment = (
  { __typename?: 'Comment' }
  & Pick<Comment, 'id' | 'text' | 'createdAt' | 'replyId'>
  & { user: (
    { __typename?: 'User' }
    & Pick<User, 'email'>
  ), replies?: Maybe<Array<(
    { __typename?: 'Comment' }
    & Pick<Comment, 'id'>
  )>> }
);

export type PostQueryVariables = Exact<{
  id: Scalars['Int'];
}>;


export type PostQuery = (
  { __typename?: 'Query' }
  & { getPost?: Maybe<(
    { __typename?: 'Post' }
    & Pick<Post, 'id' | 'title'>
    & { body: (
      { __typename?: 'postBody' }
      & BodyFragment
    ), comments: Array<(
      { __typename?: 'Comment' }
      & PostCommentFragment
    )> }
  )> }
);

export type PostsQueryVariables = Exact<{ [key: string]: never; }>;


export type PostsQuery = (
  { __typename?: 'Query' }
  & { posts: Array<(
    { __typename?: 'Post' }
    & Pick<Post, 'title' | 'id'>
    & { body: (
      { __typename?: 'postBody' }
      & BodyFragment
    ) }
  )> }
);

export type PostsPreviewsQueryVariables = Exact<{
  skip: Scalars['Int'];
  take: Scalars['Int'];
}>;


export type PostsPreviewsQuery = (
  { __typename?: 'Query' }
  & { totalItems: Query['getAmountOfPosts'] }
  & { postsPreview: Array<(
    { __typename?: 'PostPreview' }
    & Pick<PostPreview, 'bodyPreview' | 'imageSrc' | 'title' | 'id' | 'userId'>
  )> }
);

export type SearchPostsQueryVariables = Exact<{
  value: Scalars['String'];
}>;


export type SearchPostsQuery = (
  { __typename?: 'Query' }
  & { globalSearch: Array<(
    { __typename?: 'PostPreview' }
    & Pick<PostPreview, 'id' | 'imageSrc' | 'title'>
  )> }
);

export type AddPostMutationVariables = Exact<{
  title: Scalars['String'];
  body: PostBodyInput;
}>;


export type AddPostMutation = (
  { __typename?: 'Mutation' }
  & { addPost: (
    { __typename?: 'Post' }
    & Pick<Post, 'id' | 'title'>
    & { body: (
      { __typename?: 'postBody' }
      & BodyFragment
    ) }
  ) }
);

export type DeletePostMutationVariables = Exact<{
  id: Scalars['Float'];
}>;


export type DeletePostMutation = (
  { __typename?: 'Mutation' }
  & { deletePost: (
    { __typename?: 'ApiResponse' }
    & Pick<ApiResponse, 'ok'>
  ) }
);

export type LeaveCommentMutationVariables = Exact<{
  postId: Scalars['Int'];
  text: Scalars['String'];
  replyId?: Maybe<Scalars['Int']>;
}>;


export type LeaveCommentMutation = (
  { __typename?: 'Mutation' }
  & { leaveComment: (
    { __typename?: 'Comment' }
    & PostCommentFragment
  ) }
);

export type RegisterMutationVariables = Exact<{
  email: Scalars['String'];
  password: Scalars['String'];
  name: Scalars['String'];
}>;


export type RegisterMutation = (
  { __typename?: 'Mutation' }
  & { register: (
    { __typename?: 'LoginResponse' }
    & Pick<LoginResponse, 'accessToken'>
    & { user: (
      { __typename?: 'User' }
      & Pick<User, 'email' | 'id' | 'name'>
    ) }
  ) }
);

export type LoginMutationVariables = Exact<{
  email: Scalars['String'];
  password: Scalars['String'];
}>;


export type LoginMutation = (
  { __typename?: 'Mutation' }
  & { login: (
    { __typename?: 'LoginResponse' }
    & Pick<LoginResponse, 'accessToken'>
    & { user: (
      { __typename?: 'User' }
      & Pick<User, 'id' | 'email' | 'name'>
    ) }
  ) }
);

export type MeQueryVariables = Exact<{ [key: string]: never; }>;


export type MeQuery = (
  { __typename?: 'Query' }
  & { me?: Maybe<(
    { __typename?: 'User' }
    & Pick<User, 'email' | 'id'>
  )> }
);

export type UsersQueryVariables = Exact<{ [key: string]: never; }>;


export type UsersQuery = (
  { __typename?: 'Query' }
  & { users: Array<(
    { __typename?: 'User' }
    & Pick<User, 'email' | 'id' | 'name'>
  )> }
);

export type UserQueryVariables = Exact<{
  id: Scalars['Int'];
}>;


export type UserQuery = (
  { __typename?: 'Query' }
  & { getUserInfo: (
    { __typename?: 'User' }
    & Pick<User, 'id' | 'name'>
  ) }
);

export const BaseMessageFragmentDoc = gql`
    fragment baseMessage on Message {
  id
  createdAt
  updatedAt
  from {
    id
    name
  }
  text
}
    `;
export const BodyFragmentDoc = gql`
    fragment body on postBody {
  blocks {
    depth
    key
    text
    type
    entityRanges {
      key
      length
      offset
    }
    inlineStyleRanges {
      length
      offset
      style
    }
  }
  entityMap {
    data {
      src
    }
    mutability
    type
  }
}
    `;
export const PostCommentFragmentDoc = gql`
    fragment postComment on Comment {
  id
  text
  user {
    email
  }
  createdAt
  replies {
    id
  }
  replyId
}
    `;
export const HelloDocument = gql`
    query Hello {
  hello
}
    `;

/**
 * __useHelloQuery__
 *
 * To run a query within a React component, call `useHelloQuery` and pass it any options that fit your needs.
 * When your component renders, `useHelloQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useHelloQuery({
 *   variables: {
 *   },
 * });
 */
export function useHelloQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<HelloQuery, HelloQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return ApolloReactHooks.useQuery<HelloQuery, HelloQueryVariables>(HelloDocument, options);
      }
export function useHelloLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<HelloQuery, HelloQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return ApolloReactHooks.useLazyQuery<HelloQuery, HelloQueryVariables>(HelloDocument, options);
        }
export type HelloQueryHookResult = ReturnType<typeof useHelloQuery>;
export type HelloLazyQueryHookResult = ReturnType<typeof useHelloLazyQuery>;
export type HelloQueryResult = ApolloReactCommon.QueryResult<HelloQuery, HelloQueryVariables>;
export const SubscribeToMessagesDocument = gql`
    subscription SubscribeToMessages {
  subscribeToMessage {
    ...baseMessage
  }
}
    ${BaseMessageFragmentDoc}`;

/**
 * __useSubscribeToMessagesSubscription__
 *
 * To run a query within a React component, call `useSubscribeToMessagesSubscription` and pass it any options that fit your needs.
 * When your component renders, `useSubscribeToMessagesSubscription` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the subscription, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useSubscribeToMessagesSubscription({
 *   variables: {
 *   },
 * });
 */
export function useSubscribeToMessagesSubscription(baseOptions?: ApolloReactHooks.SubscriptionHookOptions<SubscribeToMessagesSubscription, SubscribeToMessagesSubscriptionVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return ApolloReactHooks.useSubscription<SubscribeToMessagesSubscription, SubscribeToMessagesSubscriptionVariables>(SubscribeToMessagesDocument, options);
      }
export type SubscribeToMessagesSubscriptionHookResult = ReturnType<typeof useSubscribeToMessagesSubscription>;
export type SubscribeToMessagesSubscriptionResult = ApolloReactCommon.SubscriptionResult<SubscribeToMessagesSubscription>;
export const GetUsersListDocument = gql`
    query GetUsersList {
  getListOfUsersWithMessages {
    id
    name
  }
}
    `;

/**
 * __useGetUsersListQuery__
 *
 * To run a query within a React component, call `useGetUsersListQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetUsersListQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetUsersListQuery({
 *   variables: {
 *   },
 * });
 */
export function useGetUsersListQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<GetUsersListQuery, GetUsersListQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return ApolloReactHooks.useQuery<GetUsersListQuery, GetUsersListQueryVariables>(GetUsersListDocument, options);
      }
export function useGetUsersListLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<GetUsersListQuery, GetUsersListQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return ApolloReactHooks.useLazyQuery<GetUsersListQuery, GetUsersListQueryVariables>(GetUsersListDocument, options);
        }
export type GetUsersListQueryHookResult = ReturnType<typeof useGetUsersListQuery>;
export type GetUsersListLazyQueryHookResult = ReturnType<typeof useGetUsersListLazyQuery>;
export type GetUsersListQueryResult = ApolloReactCommon.QueryResult<GetUsersListQuery, GetUsersListQueryVariables>;
export const SendMessageDocument = gql`
    mutation SendMessage($text: String!, $to: Int!) {
  sendMessage(text: $text, to: $to) {
    createdAt
    updatedAt
    id
    text
  }
}
    `;
export type SendMessageMutationFn = ApolloReactCommon.MutationFunction<SendMessageMutation, SendMessageMutationVariables>;

/**
 * __useSendMessageMutation__
 *
 * To run a mutation, you first call `useSendMessageMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useSendMessageMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [sendMessageMutation, { data, loading, error }] = useSendMessageMutation({
 *   variables: {
 *      text: // value for 'text'
 *      to: // value for 'to'
 *   },
 * });
 */
export function useSendMessageMutation(baseOptions?: ApolloReactHooks.MutationHookOptions<SendMessageMutation, SendMessageMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return ApolloReactHooks.useMutation<SendMessageMutation, SendMessageMutationVariables>(SendMessageDocument, options);
      }
export type SendMessageMutationHookResult = ReturnType<typeof useSendMessageMutation>;
export type SendMessageMutationResult = ApolloReactCommon.MutationResult<SendMessageMutation>;
export type SendMessageMutationOptions = ApolloReactCommon.BaseMutationOptions<SendMessageMutation, SendMessageMutationVariables>;
export const GetMessagesDocument = gql`
    query GetMessages($interlocutorId: Int!) {
  getMessages(interlocutorId: $interlocutorId) {
    ...baseMessage
  }
}
    ${BaseMessageFragmentDoc}`;

/**
 * __useGetMessagesQuery__
 *
 * To run a query within a React component, call `useGetMessagesQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetMessagesQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetMessagesQuery({
 *   variables: {
 *      interlocutorId: // value for 'interlocutorId'
 *   },
 * });
 */
export function useGetMessagesQuery(baseOptions: ApolloReactHooks.QueryHookOptions<GetMessagesQuery, GetMessagesQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return ApolloReactHooks.useQuery<GetMessagesQuery, GetMessagesQueryVariables>(GetMessagesDocument, options);
      }
export function useGetMessagesLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<GetMessagesQuery, GetMessagesQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return ApolloReactHooks.useLazyQuery<GetMessagesQuery, GetMessagesQueryVariables>(GetMessagesDocument, options);
        }
export type GetMessagesQueryHookResult = ReturnType<typeof useGetMessagesQuery>;
export type GetMessagesLazyQueryHookResult = ReturnType<typeof useGetMessagesLazyQuery>;
export type GetMessagesQueryResult = ApolloReactCommon.QueryResult<GetMessagesQuery, GetMessagesQueryVariables>;
export const PostDocument = gql`
    query Post($id: Int!) {
  getPost(id: $id) {
    id
    title
    body {
      ...body
    }
    comments {
      ...postComment
    }
  }
}
    ${BodyFragmentDoc}
${PostCommentFragmentDoc}`;

/**
 * __usePostQuery__
 *
 * To run a query within a React component, call `usePostQuery` and pass it any options that fit your needs.
 * When your component renders, `usePostQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = usePostQuery({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function usePostQuery(baseOptions: ApolloReactHooks.QueryHookOptions<PostQuery, PostQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return ApolloReactHooks.useQuery<PostQuery, PostQueryVariables>(PostDocument, options);
      }
export function usePostLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<PostQuery, PostQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return ApolloReactHooks.useLazyQuery<PostQuery, PostQueryVariables>(PostDocument, options);
        }
export type PostQueryHookResult = ReturnType<typeof usePostQuery>;
export type PostLazyQueryHookResult = ReturnType<typeof usePostLazyQuery>;
export type PostQueryResult = ApolloReactCommon.QueryResult<PostQuery, PostQueryVariables>;
export const PostsDocument = gql`
    query Posts {
  posts {
    title
    body {
      ...body
    }
    id
  }
}
    ${BodyFragmentDoc}`;

/**
 * __usePostsQuery__
 *
 * To run a query within a React component, call `usePostsQuery` and pass it any options that fit your needs.
 * When your component renders, `usePostsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = usePostsQuery({
 *   variables: {
 *   },
 * });
 */
export function usePostsQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<PostsQuery, PostsQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return ApolloReactHooks.useQuery<PostsQuery, PostsQueryVariables>(PostsDocument, options);
      }
export function usePostsLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<PostsQuery, PostsQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return ApolloReactHooks.useLazyQuery<PostsQuery, PostsQueryVariables>(PostsDocument, options);
        }
export type PostsQueryHookResult = ReturnType<typeof usePostsQuery>;
export type PostsLazyQueryHookResult = ReturnType<typeof usePostsLazyQuery>;
export type PostsQueryResult = ApolloReactCommon.QueryResult<PostsQuery, PostsQueryVariables>;
export const PostsPreviewsDocument = gql`
    query PostsPreviews($skip: Int!, $take: Int!) {
  totalItems: getAmountOfPosts
  postsPreview(skip: $skip, take: $take) {
    bodyPreview
    imageSrc
    title
    id
    userId
  }
}
    `;

/**
 * __usePostsPreviewsQuery__
 *
 * To run a query within a React component, call `usePostsPreviewsQuery` and pass it any options that fit your needs.
 * When your component renders, `usePostsPreviewsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = usePostsPreviewsQuery({
 *   variables: {
 *      skip: // value for 'skip'
 *      take: // value for 'take'
 *   },
 * });
 */
export function usePostsPreviewsQuery(baseOptions: ApolloReactHooks.QueryHookOptions<PostsPreviewsQuery, PostsPreviewsQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return ApolloReactHooks.useQuery<PostsPreviewsQuery, PostsPreviewsQueryVariables>(PostsPreviewsDocument, options);
      }
export function usePostsPreviewsLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<PostsPreviewsQuery, PostsPreviewsQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return ApolloReactHooks.useLazyQuery<PostsPreviewsQuery, PostsPreviewsQueryVariables>(PostsPreviewsDocument, options);
        }
export type PostsPreviewsQueryHookResult = ReturnType<typeof usePostsPreviewsQuery>;
export type PostsPreviewsLazyQueryHookResult = ReturnType<typeof usePostsPreviewsLazyQuery>;
export type PostsPreviewsQueryResult = ApolloReactCommon.QueryResult<PostsPreviewsQuery, PostsPreviewsQueryVariables>;
export const SearchPostsDocument = gql`
    query SearchPosts($value: String!) {
  globalSearch(value: $value) {
    id
    imageSrc
    title
  }
}
    `;

/**
 * __useSearchPostsQuery__
 *
 * To run a query within a React component, call `useSearchPostsQuery` and pass it any options that fit your needs.
 * When your component renders, `useSearchPostsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useSearchPostsQuery({
 *   variables: {
 *      value: // value for 'value'
 *   },
 * });
 */
export function useSearchPostsQuery(baseOptions: ApolloReactHooks.QueryHookOptions<SearchPostsQuery, SearchPostsQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return ApolloReactHooks.useQuery<SearchPostsQuery, SearchPostsQueryVariables>(SearchPostsDocument, options);
      }
export function useSearchPostsLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<SearchPostsQuery, SearchPostsQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return ApolloReactHooks.useLazyQuery<SearchPostsQuery, SearchPostsQueryVariables>(SearchPostsDocument, options);
        }
export type SearchPostsQueryHookResult = ReturnType<typeof useSearchPostsQuery>;
export type SearchPostsLazyQueryHookResult = ReturnType<typeof useSearchPostsLazyQuery>;
export type SearchPostsQueryResult = ApolloReactCommon.QueryResult<SearchPostsQuery, SearchPostsQueryVariables>;
export const AddPostDocument = gql`
    mutation AddPost($title: String!, $body: postBodyInput!) {
  addPost(title: $title, body: $body) {
    body {
      ...body
    }
    id
    title
  }
}
    ${BodyFragmentDoc}`;
export type AddPostMutationFn = ApolloReactCommon.MutationFunction<AddPostMutation, AddPostMutationVariables>;

/**
 * __useAddPostMutation__
 *
 * To run a mutation, you first call `useAddPostMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useAddPostMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [addPostMutation, { data, loading, error }] = useAddPostMutation({
 *   variables: {
 *      title: // value for 'title'
 *      body: // value for 'body'
 *   },
 * });
 */
export function useAddPostMutation(baseOptions?: ApolloReactHooks.MutationHookOptions<AddPostMutation, AddPostMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return ApolloReactHooks.useMutation<AddPostMutation, AddPostMutationVariables>(AddPostDocument, options);
      }
export type AddPostMutationHookResult = ReturnType<typeof useAddPostMutation>;
export type AddPostMutationResult = ApolloReactCommon.MutationResult<AddPostMutation>;
export type AddPostMutationOptions = ApolloReactCommon.BaseMutationOptions<AddPostMutation, AddPostMutationVariables>;
export const DeletePostDocument = gql`
    mutation DeletePost($id: Float!) {
  deletePost(id: $id) {
    ok
  }
}
    `;
export type DeletePostMutationFn = ApolloReactCommon.MutationFunction<DeletePostMutation, DeletePostMutationVariables>;

/**
 * __useDeletePostMutation__
 *
 * To run a mutation, you first call `useDeletePostMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useDeletePostMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [deletePostMutation, { data, loading, error }] = useDeletePostMutation({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useDeletePostMutation(baseOptions?: ApolloReactHooks.MutationHookOptions<DeletePostMutation, DeletePostMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return ApolloReactHooks.useMutation<DeletePostMutation, DeletePostMutationVariables>(DeletePostDocument, options);
      }
export type DeletePostMutationHookResult = ReturnType<typeof useDeletePostMutation>;
export type DeletePostMutationResult = ApolloReactCommon.MutationResult<DeletePostMutation>;
export type DeletePostMutationOptions = ApolloReactCommon.BaseMutationOptions<DeletePostMutation, DeletePostMutationVariables>;
export const LeaveCommentDocument = gql`
    mutation LeaveComment($postId: Int!, $text: String!, $replyId: Int) {
  leaveComment(postId: $postId, text: $text, replyId: $replyId) {
    ...postComment
  }
}
    ${PostCommentFragmentDoc}`;
export type LeaveCommentMutationFn = ApolloReactCommon.MutationFunction<LeaveCommentMutation, LeaveCommentMutationVariables>;

/**
 * __useLeaveCommentMutation__
 *
 * To run a mutation, you first call `useLeaveCommentMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useLeaveCommentMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [leaveCommentMutation, { data, loading, error }] = useLeaveCommentMutation({
 *   variables: {
 *      postId: // value for 'postId'
 *      text: // value for 'text'
 *      replyId: // value for 'replyId'
 *   },
 * });
 */
export function useLeaveCommentMutation(baseOptions?: ApolloReactHooks.MutationHookOptions<LeaveCommentMutation, LeaveCommentMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return ApolloReactHooks.useMutation<LeaveCommentMutation, LeaveCommentMutationVariables>(LeaveCommentDocument, options);
      }
export type LeaveCommentMutationHookResult = ReturnType<typeof useLeaveCommentMutation>;
export type LeaveCommentMutationResult = ApolloReactCommon.MutationResult<LeaveCommentMutation>;
export type LeaveCommentMutationOptions = ApolloReactCommon.BaseMutationOptions<LeaveCommentMutation, LeaveCommentMutationVariables>;
export const RegisterDocument = gql`
    mutation Register($email: String!, $password: String!, $name: String!) {
  register(email: $email, password: $password, name: $name) {
    accessToken
    user {
      email
      id
      name
    }
  }
}
    `;
export type RegisterMutationFn = ApolloReactCommon.MutationFunction<RegisterMutation, RegisterMutationVariables>;

/**
 * __useRegisterMutation__
 *
 * To run a mutation, you first call `useRegisterMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useRegisterMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [registerMutation, { data, loading, error }] = useRegisterMutation({
 *   variables: {
 *      email: // value for 'email'
 *      password: // value for 'password'
 *      name: // value for 'name'
 *   },
 * });
 */
export function useRegisterMutation(baseOptions?: ApolloReactHooks.MutationHookOptions<RegisterMutation, RegisterMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return ApolloReactHooks.useMutation<RegisterMutation, RegisterMutationVariables>(RegisterDocument, options);
      }
export type RegisterMutationHookResult = ReturnType<typeof useRegisterMutation>;
export type RegisterMutationResult = ApolloReactCommon.MutationResult<RegisterMutation>;
export type RegisterMutationOptions = ApolloReactCommon.BaseMutationOptions<RegisterMutation, RegisterMutationVariables>;
export const LoginDocument = gql`
    mutation Login($email: String!, $password: String!) {
  login(email: $email, password: $password) {
    accessToken
    user {
      id
      email
      name
    }
  }
}
    `;
export type LoginMutationFn = ApolloReactCommon.MutationFunction<LoginMutation, LoginMutationVariables>;

/**
 * __useLoginMutation__
 *
 * To run a mutation, you first call `useLoginMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useLoginMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [loginMutation, { data, loading, error }] = useLoginMutation({
 *   variables: {
 *      email: // value for 'email'
 *      password: // value for 'password'
 *   },
 * });
 */
export function useLoginMutation(baseOptions?: ApolloReactHooks.MutationHookOptions<LoginMutation, LoginMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return ApolloReactHooks.useMutation<LoginMutation, LoginMutationVariables>(LoginDocument, options);
      }
export type LoginMutationHookResult = ReturnType<typeof useLoginMutation>;
export type LoginMutationResult = ApolloReactCommon.MutationResult<LoginMutation>;
export type LoginMutationOptions = ApolloReactCommon.BaseMutationOptions<LoginMutation, LoginMutationVariables>;
export const MeDocument = gql`
    query Me {
  me {
    email
    id
  }
}
    `;

/**
 * __useMeQuery__
 *
 * To run a query within a React component, call `useMeQuery` and pass it any options that fit your needs.
 * When your component renders, `useMeQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useMeQuery({
 *   variables: {
 *   },
 * });
 */
export function useMeQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<MeQuery, MeQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return ApolloReactHooks.useQuery<MeQuery, MeQueryVariables>(MeDocument, options);
      }
export function useMeLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<MeQuery, MeQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return ApolloReactHooks.useLazyQuery<MeQuery, MeQueryVariables>(MeDocument, options);
        }
export type MeQueryHookResult = ReturnType<typeof useMeQuery>;
export type MeLazyQueryHookResult = ReturnType<typeof useMeLazyQuery>;
export type MeQueryResult = ApolloReactCommon.QueryResult<MeQuery, MeQueryVariables>;
export const UsersDocument = gql`
    query Users {
  users {
    email
    id
    name
  }
}
    `;

/**
 * __useUsersQuery__
 *
 * To run a query within a React component, call `useUsersQuery` and pass it any options that fit your needs.
 * When your component renders, `useUsersQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useUsersQuery({
 *   variables: {
 *   },
 * });
 */
export function useUsersQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<UsersQuery, UsersQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return ApolloReactHooks.useQuery<UsersQuery, UsersQueryVariables>(UsersDocument, options);
      }
export function useUsersLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<UsersQuery, UsersQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return ApolloReactHooks.useLazyQuery<UsersQuery, UsersQueryVariables>(UsersDocument, options);
        }
export type UsersQueryHookResult = ReturnType<typeof useUsersQuery>;
export type UsersLazyQueryHookResult = ReturnType<typeof useUsersLazyQuery>;
export type UsersQueryResult = ApolloReactCommon.QueryResult<UsersQuery, UsersQueryVariables>;
export const UserDocument = gql`
    query User($id: Int!) {
  getUserInfo(id: $id) {
    id
    name
  }
}
    `;

/**
 * __useUserQuery__
 *
 * To run a query within a React component, call `useUserQuery` and pass it any options that fit your needs.
 * When your component renders, `useUserQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useUserQuery({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useUserQuery(baseOptions: ApolloReactHooks.QueryHookOptions<UserQuery, UserQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return ApolloReactHooks.useQuery<UserQuery, UserQueryVariables>(UserDocument, options);
      }
export function useUserLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<UserQuery, UserQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return ApolloReactHooks.useLazyQuery<UserQuery, UserQueryVariables>(UserDocument, options);
        }
export type UserQueryHookResult = ReturnType<typeof useUserQuery>;
export type UserLazyQueryHookResult = ReturnType<typeof useUserLazyQuery>;
export type UserQueryResult = ApolloReactCommon.QueryResult<UserQuery, UserQueryVariables>;