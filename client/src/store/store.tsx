import { types, flow, getRoot } from 'mobx-state-tree';

import { client, reconnectToSubscriptionClient } from 'apolloClient';

import { UserModel } from 'models';
import { LoginResponse, MeDocument, MeQuery } from 'graphql/generated';

import { RootStore } from './RootStore';
import { inMemoryToken, refreshToken, getCurrentUser } from 'utils/auth';
import { request } from 'utils/request';
import { asyncFnToGenerator } from 'utils/typeCasting';

export const AppStoreModel = types
    .model({
        user: types.maybeNull(UserModel),

        initialized: types.boolean,
    })
    .actions(self => ({
        initApp: flow(function*() {
            try {
                yield refreshToken();
                const user = yield* asyncFnToGenerator(getCurrentUser)();
                if (user) {
                    self.user = UserModel.create(user);
                }
                reconnectToSubscriptionClient();
            } catch (error) {
            } finally {
                self.initialized = true;
            }
        }),
        login(response: LoginResponse) {
            if (response) {
                const { user, accessToken } = response;
                inMemoryToken.accessToken = accessToken;
                self.user = UserModel.create(user);
                const { ui } = getRoot<typeof RootStore>(self);
                ui.afterAuthCallback?.();
                ui.toggleRegistryModal(false);
                reconnectToSubscriptionClient();
                client.writeQuery<MeQuery>({
                    query: MeDocument,
                    data: {
                        me: user,
                    },
                });
            }
        },
        logout: flow(function*() {
            yield* asyncFnToGenerator(() =>
                request<{ data: null }>({
                    url: 'auth/logout',
                })
            )();
            inMemoryToken.accessToken = undefined;
            self.user = null;
            // yield client.resetStore();
            reconnectToSubscriptionClient();
        }),
        checkAuthBefore: (callback: () => void) => {
            if (!self.user) {
                const { ui } = getRoot<typeof RootStore>(self);
                ui.toggleRegistryModal(true, callback);
            } else {
                callback();
            }
        },
    }));
