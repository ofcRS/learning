export {
    useRootStore as useStore,
    RootStoreProvider,
    rootStore,
} from './RootStore';
