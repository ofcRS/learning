declare module '*.svg' {
    const content: string;
    export default content;
}

declare module '*.jpg' {
    const value: string;
    export = value;
}

declare module '*.woff2' {
    const value: string;
    export = value;
}
