export type Record<T> = T & {
    id: number
}