# Устанавливаем docker и docker-compose
if ! [ -x "$(command -v docker)" ]; then
  sudo apt update
  sudo apt install apt-transport-https ca-certificates curl gnupg
  curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
  echo \  "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
    $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
  sudo apt update
  sudo apt install docker-ce docker-ce-cli containerd.io

  sudo curl -L "https://github.com/docker/compose/releases/download/1.28.5/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
  sudo chmod +x /usr/local/bin/docker-compose
  sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose
fi

# Указываем адрес сервера
printf "Enter server address: "
read -r address;
touch .env;
echo "EXTERNAL_URL=${address}" > .env;

# Инициализируем приватные данные, если еще не
SECRETS=(db_password jwt_secret refresh_jwt_secret);

if ! [[ -d ./secret ]]
then
  mkdir secret;
  cd secret;

  for secret in ${SECRETS[@]}; do
    touch $secret;

    printf "Enter ${secret}: "
    read -r store;

    echo $store > ./$secret;
  done;
fi