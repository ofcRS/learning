import fs from 'fs';

import { SecretData } from 'app';

export class SecretDataImplementation implements SecretData {
    private readonly databasePassword: string;
    private readonly jwtSecret: string;
    private readonly refreshJwtSecret: string;

    private readonly parseEnv = (variable: string) => {
        const read = process.env[variable] as string;
        return fs.existsSync(read) ? fs.readFileSync(read).toString() : read;
    };

    constructor() {
        this.databasePassword = this.parseEnv('PG_PASSWORD');
        this.jwtSecret = this.parseEnv('JWT_SECRET');
        this.refreshJwtSecret = this.parseEnv('REFRESH_JWT_SECRET');
    }

    getDatabasePassword = () => this.databasePassword;
    getJwtSecret = () => this.jwtSecret;
    getRefreshJwtSecret = () => this.refreshJwtSecret;
}
