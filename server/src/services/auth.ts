import { Response } from 'express';
import { sign } from 'jsonwebtoken';
import { User } from 'entities';
import { AUTH_COOKIES_PATH } from '../config/network';
import { App } from '../app';
import { MiddlewareFn } from 'type-graphql';
import { Context, ContextPayload } from '../types/services/context';
import * as jwt from 'jsonwebtoken';

export class AuthService {
    static createAccessToken = (user: User): string =>
        sign(
            {
                id: user.id,
                version: user.tokenVersion,
            },
            App.secretData.getJwtSecret(),
            {
                expiresIn: '10s',
            }
        );

    static createRefreshToken = (user: User): string =>
        sign(
            {
                id: user.id,
                version: user.tokenVersion,
            },
            App.secretData.getRefreshJwtSecret(),
            {
                expiresIn: '365d',
            }
        );

    static sendRefreshToken = (res: Response, token: string) =>
        res.cookie('jid', token, {
            httpOnly: true,
            path: AUTH_COOKIES_PATH,
        });

    static checkAuth: MiddlewareFn<Context> = ({ context }, next) => {
        try {
            const secret = App.secretData.getJwtSecret();
            const token = context.req.headers.authorization;

            if (!token) {
                throw new Error("there's no token in headers");
            }

            const payload = jwt.verify(token, secret) as ContextPayload;
            context.payload = payload;
        } catch (error) {
            throw new Error(error.message);
        }
        return next();
    };

    static checkWSAuth = async (connectionParams: {
        authorization?: string;
    }): Promise<Record<string, unknown> | undefined> => {
        const token = connectionParams?.authorization;
        if (token) {
            const secret = App.secretData.getJwtSecret();
            return jwt.verify(token, secret) as ContextPayload;
        }
        return undefined;
    };
}
