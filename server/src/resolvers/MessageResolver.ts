import {
    Mutation,
    PubSub,
    Resolver,
    Subscription,
    PubSubEngine,
    Arg,
    Ctx,
    UseMiddleware,
    Root,
    Int,
    Query,
} from 'type-graphql';
import { createQueryBuilder } from 'typeorm';

import { Context } from 'types/services/context';
import { AuthService } from 'services/auth';
import { Message } from 'entities/message';
import { User } from '../entities';

enum Topics {
    NOTIFICATIONS = 'NOTIFICATIONS',
}

type MessageNotification = {
    messageId: number;
    messageRecipientId: number;
};

@Resolver()
export class MessageResolver {
    /* Получить историю сообщений от пользователя  */
    @Query(() => [Message])
    @UseMiddleware(AuthService.checkAuth)
    async getMessages(
        @Arg('interlocutorId', () => Int) interlocutorId: number,
        @Ctx() { payload }: Context
    ): Promise<Message[]> {
        if (!payload) return [];

        const result = await createQueryBuilder(Message, 'message')
            .leftJoinAndSelect('message.from', 'from')
            .where(
                `(message.fromUserId = :user1 and message.toUserId = :user2) or 
                        (message.fromUserId = :user2 and message.toUserId = :user1)`,
                {
                    user1: interlocutorId,
                    user2: payload.id,
                }
            )
            .getMany();
        console.log(result);

        return result;
    }

    /* Получить список пользователей с которыми есть диалоги */
    @Query(() => [User])
    @UseMiddleware(AuthService.checkAuth)
    async getListOfUsersWithMessages(
        @Ctx() { payload }: Context
    ): Promise<User[]> {
        if (!payload) return [];

        return createQueryBuilder()
            .select('*')
            .from(User, 'u')
            .leftJoin('u.hasMessagesWith', 'm')
            .where('u_m.userId_1 = :id', { id: payload.id })
            .getRawMany<User>();

        // return createQueryBuilder()
        //     .select('*')
        //     .distinctOn(['u.email'])
        //     .from(Message, 'm')
        //     .innerJoin('user', 'u', 'u.id = m.fromUserId')
        //     .where('m.fromUserId = :id or m.toUserId = :id', { id: payload.id })
        //     .getRawMany<User>();
    }

    /* Отправить сообщение пользователю */
    @Mutation(() => Message)
    @UseMiddleware(AuthService.checkAuth)
    async sendMessage(
        @Arg('text') text: string,
        @Arg('to', () => Int) to: number,
        @Ctx() { payload }: Context,
        @PubSub() pubSub: PubSubEngine
    ): Promise<Message> {
        if (!payload) throw Error('You must be authorized');

        const fromUser = await User.findOne(payload.id, {
            relations: ['hasMessagesWith'],
        });
        const toUser = await User.findOne(to, {
            relations: ['hasMessagesWith'],
        });

        if (!fromUser || !toUser) throw Error('Wrong recipient');

        /* Добавляю запись о том что сообщение было в таблицу,
         * чтобы потом получать список пользователей с которыми есть диалог
         *  */
        fromUser.hasMessagesWith.push(toUser);
        await fromUser.save();
        toUser.hasMessagesWith.push(fromUser);
        await toUser.save();

        const message = new Message();
        message.fromUserId = payload.id;
        message.toUserId = to;
        message.text = text;

        await message.save();

        const messageNotification: MessageNotification = {
            messageId: message.id,
            messageRecipientId: to,
        };

        await pubSub.publish(Topics.NOTIFICATIONS, messageNotification);
        return message;
    }

    /* Подписаться на новые сообщения */
    @Subscription(() => Message, {
        topics: Topics.NOTIFICATIONS,
        filter: ({ payload, context }) =>
            payload.messageRecipientId === context.id,
    })
    async subscribeToMessage(
        @Root() { messageId }: MessageNotification
    ): Promise<Message> {
        return await createQueryBuilder(Message, 'message')
            .leftJoinAndSelect('message.from', 'from')
            .where('message.id = :id', { id: messageId })
            .getOneOrFail();
    }
}
