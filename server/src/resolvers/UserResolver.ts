import {
    Arg,
    Ctx,
    Field, Int,
    Mutation,
    ObjectType,
    Query,
    Resolver,
    UseMiddleware,
} from 'type-graphql';
import { compare, hash } from 'bcryptjs';
import * as jwt from 'jsonwebtoken';
import { User } from 'entities/user';
import { getConnection } from 'typeorm';
import { Context, ContextPayload } from '../types/services/context';
import { AuthService } from '../services/auth';
import { App } from 'app';

@ObjectType()
class LoginResponse {
    @Field()
    accessToken: string;

    @Field()
    user: User;
}

@Resolver()
export class UserResolver {
    @Query(() => String)
    hello(): string {
        return 'hi!';
    }

    @Query(() => [User])
    users() {
        return User.find();
    }

    @Mutation(() => Boolean) async revokeRefreshTokens(@Arg('id') id: number) {
        await getConnection()
            .getRepository(User)
            .increment({ id }, 'tokenVersion', 1);
        return true;
    }

    @Mutation(() => LoginResponse)
    async login(
        @Arg('email') email: string,
        @Arg('password') password: string,
        @Ctx() { res }: Context
    ): Promise<LoginResponse> {
        new User();
        const user = await User.findOne({
            where: {
                email,
            },
        });

        if (!user) {
            throw new Error('User not found');
        }

        const valid = await compare(password, user.password);

        if (!valid) {
            throw new Error('Password is incorrect');
        }

        AuthService.sendRefreshToken(res, AuthService.createRefreshToken(user));

        return {
            accessToken: AuthService.createAccessToken(user),
            user,
        };
    }

    @Mutation(() => LoginResponse)
    async register(
        @Arg('email') email: string,
        @Arg('password') password: string,
        @Arg('name') name: string,
        @Ctx() { res }: Context
    ): Promise<LoginResponse> {
        try {
            const userWithSameEmail = await User.findOne({
                where: {
                    email,
                },
            });

            if (userWithSameEmail) {
                throw new Error('User already exists');
            }
            const hashedPassword = await hash(password, 10);

            const user = new User();

            user.password = hashedPassword;
            user.email = email;
            user.name = name;

            await user.save();

            AuthService.sendRefreshToken(
                res,
                AuthService.createRefreshToken(user)
            );
            return { accessToken: AuthService.createAccessToken(user), user };
        } catch (error) {
            throw error;
        }
    }

    @Query(() => User, { nullable: true })
    async me(@Ctx() { req }: Context): Promise<User | null> {
        try {
            const token = req.headers.authorization;
            if (!token) throw new Error('no token in headers');

            const payload = jwt.verify(
                token,
                App.secretData.getJwtSecret()
            ) as ContextPayload;

            if (!payload.id) return null;

            const user = await User.findOne({ id: payload.id });
            if (!user) return null;
            return user;
        } catch (error) {
            throw error;
        }
    }

    @UseMiddleware(AuthService.checkAuth)
    @Query(() => User)
    async getUserInfo(@Arg('id', () => Int) id: number): Promise<User> {
        return User.findOneOrFail(id);
    }
}
