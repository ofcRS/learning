import { App } from 'app';

export type AbstractResolverConstructorParams = App;

export type BaseResolver = {
    getResolver: () => Function;
};

export class BaseResolverImplementation {
    app: App;

    constructor(app: AbstractResolverConstructorParams) {
        this.app = app;
    }
}
