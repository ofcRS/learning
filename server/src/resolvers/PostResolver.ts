import { getConnection } from 'typeorm';

import {
    Query,
    Resolver,
    Mutation,
    Arg,
    UseMiddleware,
    Int,
    Ctx,
} from 'type-graphql';

import { Post, PostBody, PostPreview, Comment } from 'entities';
import { ApiResponse } from 'utils/ApiHandler';

import { Context } from 'types/services/context';
import { AuthService } from '../services/auth';

@Resolver()
export class PostResolver {
    @Query(() => [Post])
    posts() {
        return Post.find({
            order: {
                id: 'DESC',
            },
        });
    }

    @Query(() => Post, {
        nullable: true,
    })
    async getPost(@Arg('id', () => Int) id: number) {
        const res = await Post.findOne(
            {
                id,
            },
            {
                relations: ['comments', 'comments.user', 'comments.replies'],
            }
        );
        console.log(JSON.stringify(res, null, '\t'));
        return res;
    }

    @Mutation(() => Post)
    @UseMiddleware(AuthService.checkAuth)
    async addPost(
        @Ctx() { payload }: Context<true>,
        @Arg('title') title: string,
        @Arg('body') body: PostBody
    ): Promise<Post> {
        const newPost = new Post();

        newPost.title = title;
        newPost.body = body;
        newPost.userId = payload.id;

        await newPost.save();
        return newPost;
    }

    @Mutation(() => ApiResponse)
    @UseMiddleware(AuthService.checkAuth)
    async deletePost(@Arg('id') id: number): Promise<ApiResponse> {
        await Post.delete(id);
        return {
            data: id,
            ok: true,
        };
    }

    static getPostPreview({ body, title, id, userId }: Post): PostPreview {
        const atomicType = body.blocks.find(
            ({ type, entityRanges }) =>
                type === 'atomic' && entityRanges.length === 1
        );

        let previewImage: null | string = null;

        if (atomicType) {
            const [{ key }] = atomicType.entityRanges;
            const { src } = body.entityMap[key].data;
            previewImage = src;
        }

        return {
            title,
            bodyPreview: body.blocks?.[0].text,
            imageSrc: previewImage,
            id,
            userId,
        };
    }

    @Query(() => [PostPreview])
    async postsPreview(
        @Arg('skip', () => Int) skip: number,
        @Arg('take', () => Int) take: number
    ): Promise<PostPreview[]> {
        const posts = await Post.find({
            skip,
            take,
        });

        return posts.map(PostResolver.getPostPreview);
    }

    @Query(() => Int)
    getAmountOfPosts(): Promise<number> {
        return Post.count();
    }

    @Mutation(() => Comment, {
        name: 'leaveComment',
    })
    @UseMiddleware(AuthService.checkAuth)
    async leaveComment(
        @Ctx() { payload }: Context<true>,
        @Arg('text') text: string,
        @Arg('postId', () => Int) postId: number,
        @Arg('replyId', () => Int, { nullable: true }) replyId: number
    ) {
        const comment = new Comment();

        comment.userId = payload.id;
        comment.text = text;
        comment.postId = postId;
        comment.replyId = replyId;

        const { id } = await comment.save();

        return Comment.findOne(id);
    }

    @Query(() => [PostPreview], { name: 'globalSearch' })
    async searchOnPosts(@Arg('value') value: string): Promise<PostPreview[]> {
        const posts = await getConnection()
            .getRepository(Post)
            .createQueryBuilder('post')
            .leftJoinAndSelect('post.user', 'user')
            .where('user.email ilike :value or post.title ilike :value', {
                value: `%${value}%`,
            })
            .getMany();
        // const posts = await Post.find({
        //     relations: ['user'],
        //     where: {
        //         title: ILike(`%${value}%`),
        //         user: {
        //             email: ILike(`%${value}%`),
        //         },
        //     },
        // });
        return posts.map(PostResolver.getPostPreview);
    }
}
