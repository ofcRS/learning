import express from 'express';

import PinoHttp from 'pino-http';
import { ApolloServer } from 'apollo-server-express';
import { buildSchema } from 'type-graphql';
import { ControllerImplementation } from 'controllers/controller';

import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';

import { Logger } from 'types/services/logger';

import { MessageResolver, PostResolver, UserResolver } from './resolvers';
import { SecretDataImplementation } from './services/secretData';
import { createConnection } from 'createConnection';
import http from 'http';
import { AuthService } from './services/auth';

type Conf = {
    port: number;
    logger: Logger;
    httpLogger: PinoHttp.HttpLogger;
    controllers: ControllerImplementation[];
};

export type SecretData = {
    getJwtSecret: () => string;
    getRefreshJwtSecret: () => string;

    getDatabasePassword: () => string;
};

export class App {
    private readonly app: express.Application;
    private readonly httpServer: http.Server;
    private readonly port: number;
    private readonly logger: Logger;
    static readonly secretData: SecretData = new SecretDataImplementation();

    // временно оставлю прямой импорт из пакета, потом напишу болле общий интерфейс для http логгера
    private readonly httpLogger: PinoHttp.HttpLogger;

    constructor({ httpLogger, logger, port, controllers }: Conf) {
        this.app = express();
        this.httpServer = http.createServer(this.app);
        this.port = port;
        this.logger = logger;
        this.httpLogger = httpLogger;

        App.initializeDatabase();
        this.initializeMiddlewares();
        this.initializeApi(controllers);
        this.initializeApollo();
    }

    private static getClientUrl = () =>
        `http://${process.env.HOST}${
            process.env.CLIENT_PORT !== undefined
                ? ':' + process.env.CLIENT_PORT
                : ''
        }`;

    private initializeMiddlewares() {
        this.app.use(bodyParser.json());
        this.app.use(cookieParser());
        this.app.use((req, res, next) => {
            res.set({
                'Access-Control-Allow-Origin': App.getClientUrl(),
                'Access-Control-Allow-Methods': '*, DELETE',
                'Access-Control-Allow-Headers':
                    'Origin, X-Requested-With, Content-Type, Accept, Authorization',
                'Access-Control-Allow-Credentials': true,
            });
            next();
        });
        this.app.use(this.httpLogger);
    }

    private initializeApi(controllers: ControllerImplementation[]) {
        controllers.forEach(controller => {
            this.app.use('/api', controller.router);
        });
    }

    private static async initializeDatabase() {
        try {
            const { name } = await createConnection();
            console.log('was succesfully connected to "' + name + '"');
        } catch (error) {
            console.log(
                'error occured while trying to connect db: ' + error.message
            );
        }
    }

    private async initializeApollo() {
        const apolloServer = new ApolloServer({
            subscriptions: {
                path: '/subscriptions',
            },
            schema: await buildSchema({
                resolvers: [UserResolver, PostResolver, MessageResolver],
            }),
            context: ({ req, res, connection }) => {
                if (connection) {
                    return AuthService.checkWSAuth(connection.context);
                } else {
                    return {
                        req,
                        res,
                    };
                }
            },
        });

        apolloServer.installSubscriptionHandlers(this.httpServer);

        apolloServer.applyMiddleware({
            app: this.app,
            cors: {
                credentials: true,
                origin: App.getClientUrl(),
            },
        });
    }

    public listen() {
        this.httpServer.listen(this.port, () => {
            console.log(`[[WAS UPDATED]] ${Date.now()}`);
            console.log(`[[STARTED ON]]: ${this.port} PORT`);
        });
    }
}
