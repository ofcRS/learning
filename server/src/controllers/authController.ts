import { verify } from 'jsonwebtoken';
import { Request, Response } from 'express';
import { OAuth2Client } from 'google-auth-library';

import { User } from 'entities';
import { ContextPayload } from 'types/services/context';

import { AuthService } from 'services/auth';

import { AUTH_COOKIES_PATH } from '../config/network';
import { ControllerImplementation } from './controller';
import { ApiHandler } from 'utils/ApiHandler';
import { App } from '../app';

type CurrentUser = {
    user: User | null;
    error: string | null;
};

export class AuthController extends ControllerImplementation {
    readonly refreshTokenCookieName = 'jid';
    readonly accessTokenHeader = 'authorization';
    readonly googleClient = new OAuth2Client(
        '399052810448-u92l9fviei84v1l83pgaesepqq94emh4.apps.googleusercontent.com'
    );

    constructor() {
        super('/auth');
        this.initializeRoutes();
    }

    initializeRoutes() {
        this.router.get(this.getApiPath('/refresh_token'), this.refreshToken);
        this.router.get(this.getApiPath('/current'), this.getCurrentUser);
        this.router.get(this.getApiPath('/logout'), this.logout);
        this.router.post(this.getApiPath('/google'), this.googleAuth);
    }

    // получить текущего пользователя из рефреш или акксесс токена
    private static async getCurrentUser(
        token: string | null,
        secret: string
    ): Promise<CurrentUser> {
        const result: CurrentUser = {
            error: null,
            user: null,
        };

        if (!token) {
            result.error = 'No token was provided';
            return result;
        }
        const payload: ContextPayload | null = verify(
            token,
            secret
        ) as ContextPayload;

        const user = await User.findOne({ id: payload.id });

        if (!user) {
            result.error = "User wasn't fount";
            return result;
        }

        if (user.tokenVersion !== payload.version) {
            result.error = 'Tokens versions are different';
            return result;
        }

        result.user = user;
        return result;
    }

    private refreshToken = async (req: Request, res: Response) => {
        try {
            const currentUser = await AuthController.getCurrentUser(
                this.getRefreshToken(req),
                App.secretData.getRefreshJwtSecret()
            );
            if (!currentUser.user) {
                return ApiHandler.sendError(res, {
                    message: currentUser.error,
                    status: 401,
                });
            }
            AuthService.sendRefreshToken(
                res,
                AuthService.createRefreshToken(currentUser.user)
            );
            ApiHandler.sendSuccessResponse(res, {
                accessToken: AuthService.createAccessToken(currentUser.user),
            });
        } catch (error) {
            return ApiHandler.sendError(res, { ...error, status: 400 });
        }
    };

    private getCurrentUser = async (req: Request, res: Response) => {
        const currentUser = await AuthController.getCurrentUser(
            this.getAccessToken(req),
            App.secretData.getJwtSecret()
        );
        if (!currentUser.user) {
            return ApiHandler.sendError(res, {
                status: 401,
                message: currentUser.error,
            });
        }
        ApiHandler.sendSuccessResponse(res, {
            user: currentUser.user,
        });
    };

    private logout = async (req: Request, res: Response) => {
        res.clearCookie('jid', {
            path: AUTH_COOKIES_PATH,
        });
        ApiHandler.sendSuccessResponse(res, null);
    };

    private googleAuth = async (req: Request, res: Response) => {
        const { token } = req.body;

        const ticket = await this.googleClient.verifyIdToken({
            idToken: token,
            audience:
                '399052810448-u92l9fviei84v1l83pgaesepqq94emh4.apps.googleusercontent.com',
        });

        const payload = await ticket?.getPayload();

        if (!payload || !payload.email) {
            return res.send({
                ok: false,
            });
        }

        const existingUser = await User.findOne({
            where: {
                email: payload.email,
            },
        });

        if (existingUser) {
            existingUser.name = payload.name || existingUser.name;
            await existingUser.save();
            AuthService.sendRefreshToken(
                res,
                AuthService.createRefreshToken(existingUser)
            );

            return ApiHandler.sendSuccessResponse(res, {
                accessToken: AuthService.createAccessToken(existingUser),
                user: existingUser,
            });
        } else {
            const user = new User();
            user.name = payload.name || '';
            user.email = payload.email;

            await user.save();

            AuthService.sendRefreshToken(
                res,
                AuthService.createRefreshToken(user)
            );
            return ApiHandler.sendSuccessResponse(res, {
                accessToken: AuthService.createAccessToken(user),
                user,
            });
        }

        res.status(201).json({
            love: 'you',
        });
    };

    private getRefreshToken(req: Request): string | null {
        return req.cookies[this.refreshTokenCookieName] || null;
    }
    private getAccessToken(req: Request): string | null {
        return req.headers[this.accessTokenHeader] || null;
    }
}
