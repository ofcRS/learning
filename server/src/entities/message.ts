import {
    BaseEntity,
    Column,
    Entity,
    JoinColumn,
    PrimaryGeneratedColumn,
    CreateDateColumn,
    UpdateDateColumn,
    ManyToOne,
} from 'typeorm';
import { Field, ObjectType, Int } from 'type-graphql';
import { User } from './user';

@ObjectType('sendMessageType')
export class SentMessage {
    @Field(() => String)
    text: string;

    @Field(() => Int)
    toUserId: number;
}

@Entity()
@ObjectType()
export class Message extends BaseEntity {
    @PrimaryGeneratedColumn()
    @Field(() => Int)
    id: number;

    @Column()
    fromUserId: number;

    @Column()
    toUserId: number;

    @ManyToOne(
        () => User,
        user => user.sentMessages,
        {
            onDelete: 'CASCADE',
        }
    )
    @JoinColumn({ name: 'fromUserId' })
    @Field(() => User)
    from: User;

    @ManyToOne(
        () => User,
        user => user.receivedMessages,
        {
            onDelete: 'CASCADE',
        }
    )
    @JoinColumn({ name: 'toUserId' })
    @Field(() => User)
    to: User;

    @Column()
    @Field()
    text: string;

    @CreateDateColumn()
    @Field()
    createdAt: Date;

    @UpdateDateColumn()
    @Field()
    updatedAt: Date;
}
