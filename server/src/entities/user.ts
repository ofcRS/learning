import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    OneToMany,
    BaseEntity,
    ManyToMany,
    JoinTable,
} from 'typeorm';
import { Field, ObjectType, Int } from 'type-graphql';

import { Post } from './post';
import { Comment } from './comment';
import { Message } from './message';

@ObjectType()
@Entity()
export class User extends BaseEntity {
    @Field(() => Int)
    @PrimaryGeneratedColumn()
    id: number;

    @Field()
    @Column()
    email: string;

    @Field()
    @Column()
    name: string;

    @Column({ nullable: true })
    password: string;

    @OneToMany(
        () => Post,
        post => post.user
    )
    posts: Post[];

    @Column('int', { default: 0 })
    tokenVersion: number;

    @OneToMany(
        () => Comment,
        comment => comment.user
    )
    comments: Comment[];

    @OneToMany(
        () => Message,
        message => message.from
    )
    sentMessages: Message[];

    @OneToMany(
        () => Message,
        message => message.to
    )
    receivedMessages: Message[];

    @ManyToMany(() => User)
    @JoinTable()
    hasMessagesWith: User[];
}
