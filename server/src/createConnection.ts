import {
    Connection,
    getConnectionOptions,
    createConnection as createTypeOrmConnection,
} from 'typeorm';

const importAllEntities = (requireContext: __WebpackModuleApi.RequireContext) =>
    requireContext
        .keys()
        .sort()
        .map((filename: string) => {
            const required = requireContext(filename);
            return Object.keys(required).reduce((result, exportedKey) => {
                const exported = required[exportedKey];
                if (typeof exported === 'function') {
                    return result.concat(exported);
                }
                return result;
            }, []);
        })
        .flat();

const entitiesViaWebpack = importAllEntities(
    require.context('./entities', true, /\.ts$/)
);

export const createConnection = async (): Promise<Connection> => {
    const baseConnectionOptions = await getConnectionOptions();
    const connectionOptions = {
        ...baseConnectionOptions,
        entities: entitiesViaWebpack,
    };
    return createTypeOrmConnection(connectionOptions);
};
