const fs = require('fs');
const { join } = require('path');

module.exports = (() => {
    const entitiesPath = join(__dirname, '/src/entities/*.{js,ts}');

    console.log('[ENTITIES PATH] - ', entitiesPath);

    const pathToPassword = process.env.PG_PASSWORD;
    const password = fs.existsSync(pathToPassword)
        ? fs.readFileSync(pathToPassword)
        : pathToPassword; /* App.secretData.getDatabasePassword();*/

    return {
        type: 'postgres',
        host: process.env.PG_HOST,
        port: process.env.PG_PORT,
        username: process.env.PG_USERNAME,
        password,
        database: process.env.PG_DATABASE,
        entities: [entitiesPath],
        synchronize: true,
        logging: ['query', 'error'],
    };
})();
