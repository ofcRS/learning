### Небольшой пет-блог :)
http://shck.xyz - билд

`docker-compose up -d --build` - запустить локально. Нужно установить следующие
переменные окружения:

- JWT_SECRET - _доступ к access токену_
- REFRESH_JWT_SECRET - _доступ к рефреш токену_
- DB_PASSWORD - _пароль от дб_
- SERVER_ADDRESS - _адрес сервера(для работы корс), можно установить, или localhost, или адресс в локальной сети, например для доступа с моб устройства_ 

`docker-compose -f docker-compose.dev.yml up -d --build` - запустить в дев режиме с поддержкой хот релоад, нужно установить SERVER_IP=localhost
